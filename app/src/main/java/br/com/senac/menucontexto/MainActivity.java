package br.com.senac.menucontexto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listViewAlunos ;
    private ArrayAdapter<Aluno> adapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* buscando elementos na view */
        listViewAlunos = findViewById(R.id.listViewAlunos);

        /* criando e populando lista */
        Aluno aluno1 = new Aluno("Jose da Silva" ,"(27)9999-9999" , "jose@jose.com.br" , "www.senac.com.br");

        Aluno aluno2 = new Aluno("Manuel Santos" ,"(27)9999-9999" , "manuel@manuel.com.br" , "www.senac.com.br");

        Aluno aluno3 = new Aluno("jonas Carvalho" ,"(27)9999-9999" , "jonas@jonas.com.br" , "www.senac.com.br");

        List<Aluno> lista = new ArrayList<>() ;

        lista.add(aluno1);
        lista.add(aluno2);
        lista.add(aluno3);

        /* criando adapter */

        adapter = new ArrayAdapter<Aluno>(this , android.R.layout.simple_list_item_1 , lista) ;

        /* definindo o adapter do listview */

        listViewAlunos.setAdapter(adapter);


    }





}
